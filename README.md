# Schéma évènements locaux

Spécifications des données essentielles des évènements locaux des collectivités et établissements publics locaux

## Contexte

Ce schéma propose un standard pour publier les évènements locaux organisés ou diffusés par les collectivités locales et établissements publics locaux.

## Documents de cadrage juridique


## Document de cadrage technique

​Format pour la publication des données essentielles des évènements locaux. Ce schéma vise à simplifier la mise à disposition des données événementielles des collectivités locales.

## Outils

* [![](https://scdl.opendatafrance.net/docs/assets/validata-logo-horizontal.png)](https://go.validata.fr/table-schema?schema_name=scdl.evenement-local) [Valider un fichier avec Validata](http://go.validata.fr/table-schema?schema_name=scdl.evenement_local)
* [Créer un fichier avec CSV Good Generator](https://csv-gg.etalab.studio/?schema=)


## Voir aussi

* adresser un message à [scdl@opendatafrance.email](mailto:scdl@opendatafrance.email?subject=evenement-local)
* ouvrir un ticket sur le [dépôt GitLab d’OpenDataFrance](https://gitlab.com/opendatafrance/scdl/schema-evenements-locaux/issues)
