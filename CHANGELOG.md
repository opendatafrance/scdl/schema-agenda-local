# Changelog

## 0.2
Migration de l'instance gitlab d'hébergement du schéma agenda local vers l'instance publique https://gitlab.com/opendatafrance/scdl/agenda

## 0.1

Création à partir des spécifications d'openAgenda et de l'entité Event de schema.org